// https://leetcode.com/problems/shortest-distance-to-a-character/description/
// Date: 2018-07-06
/*
* Given a string S and a character C, return an array of integers representing the shortest distance from the character C in the string.
* Example 1:
* Input: S = "loveleetcode", C = 'e'
* Output: [3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0]
* Note:
* 1. S string length is in [1, 10000].
* 2. C is a single character, and guaranteed to be in string S.
* 3. All letters in S and C are lowercase.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int* shortestToChar(char* S, char C, int* returnSize) {
    *returnSize=strlen(S);
    int* ret=(int*)malloc((*returnSize)*sizeof(int));
    for(int i=0;i<*returnSize;++i){
        ret[i]=(S[i]==C)?0:10001;
    }
    for(int i=0;i<*returnSize;++i){
        if(ret[i]==0){
            for(int j=0;j<*returnSize;++j){
                if(ret[j]!=0){
                    ret[j]=(abs(i-j)<ret[j])?abs(i-j):ret[j];
                }
            }
        }
    }
    return ret;
}

int main(void)
{
    const char* S="loveleetcode";
    int returnSize;
    int* ret=shortestToChar((char*)S,'e',&returnSize);
    for(int i=0;i<returnSize;++i){
        printf("%d ",ret[i]);
    }
    free(ret);
    ret=NULL;
    return 0;
}
