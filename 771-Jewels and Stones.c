// https://leetcode.com/problems/jewels-and-stones/description/
// Date: 2018-06-25
/*
* You're given strings J representing the types of stones that are jewels, and S representing the stones you have.
* Each character in S is a type of stone you have. You want to know how many of the stones you have are also jewels.
* The letters in J are guaranteed distinct, and all characters in J and S are letters. Letters are case sensitive, so "a" is considered a different type of stone from "A".
* Example 1:
* Input: J = "aA", S = "aAAbbbb"
* Output: 3
* Example 2:
* Input: J = "z", S = "ZZ"
* Output: 0
* Note:
* S and J will consist of letters and have length at most 50.
* The characters in J are distinct.
*/

#include <stdio.h>
#include <string.h>

int numJewelsInStones(char* J, char* S) {
    char flag[128];
    memset(flag,0,sizeof(flag));
    for(int i=0;J[i]!='\0';++i){
        flag[(int)J[i]]=1;
    }
    int cnt=0;
    for(int i=0;S[i]!='\0';++i){
        if(flag[(int)S[i]]){
            ++cnt;
        }
    }
    return cnt;
}

int main(void)
{
    const char *J="aA",*S="aAAbbbb";
    printf("%d\n",numJewelsInStones((char*)J,(char*)S));
    return 0;
}
