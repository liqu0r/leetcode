// https://leetcode.com/problems/two-sum/description/
// Date: 2018-06-25
/*
* Given an array of integers, return indices of the two numbers such that they add up to a specific target.
* You may assume that each input would have exactly one solution, and you may not use the same element twice.
* Example:
* Given nums = [2, 7, 11, 15], target = 9,
* Because nums[0] + nums[1] = 2 + 7 = 9,
* return [0, 1].
*/

#include <stdio.h>
#include <stdlib.h>

int* twoSum(int* nums, int numsSize, int target) {
    int* ret=(int*)malloc(2*sizeof(int));
    for(int i=0;i<numsSize;++i){
        for(int j=i+1;j<numsSize;++j){
            if(nums[i]+nums[j]==target){
                ret[0]=i;
                ret[1]=j;
                return ret;
            }
        }
    }
    return NULL;
}

int main(void)
{
    int nums[4]={2,7,11,15};
    int* ret=twoSum(nums,4,9);
    printf("%d %d\n",ret[0],ret[1]);
    free(ret);
    ret=NULL;
    return 0;
}
