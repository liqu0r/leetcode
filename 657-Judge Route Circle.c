// https://leetcode.com/problems/judge-route-circle/description/
// Date: 2018-06-29
/*
* Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle, which means it moves back to the original place.
* The move sequence is represented by a string. And each move is represent by a character. The valid robot moves are R (Right), L (Left), U (Up) and D (down).
* The output should be true or false representing whether the robot makes a circle.
* Example 1:
* Input: "UD"
* Output: true
* Example 2:
* Input: "LL"
* Output: false
*/

#include <stdio.h>
#include <stdbool.h>

bool judgeCircle(char* moves) {
    int cnt[2]={0,0};
    for(int i=0;moves[i]!='\0';++i){
        switch(moves[i]){
            case 'U':{++cnt[0];break;}
            case 'D':{--cnt[0];break;}
            case 'L':{++cnt[1];break;}
            case 'R':{--cnt[1];break;}
        }
    }
    return (cnt[0]==cnt[1]&&cnt[0]==0)?true:false;
}

int main(void)
{
    const char* moves="UD";
    printf("%s\n",(judgeCircle((char*)moves)==true)?"true":"false");
    return 0;
}
