// https://leetcode.com/problems/hamming-distance/description/
// Date: 2018-06-27
/*
* The Hamming distance between two integers is the number of positions at which the corresponding bits are different.
* Given two integers x and y, calculate the Hamming distance.
* Note:
* 0 <= x, y < 2^31.
* Example:
* Input: x = 1, y = 4
* Output: 2
* Explanation:
* 1   (0 0 0 1)
* 4   (0 1 0 0)
*        ↑   ↑
* The above arrows point to positions where the corresponding bits are different.
*/

#include <stdio.h>

int hammingDistance(int x, int y) {
    int n=x^y,cnt=0;
    for(int i=0;i<32;++i){
        cnt+=(n&(1<<i))?1:0;
    }
    return cnt;
}

int main(void)
{
    printf("%d\n",hammingDistance(1,4));
    return 0;
}
