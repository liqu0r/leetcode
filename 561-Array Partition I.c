// https://leetcode.com/problems/array-partition-i/description/
// Date: 2018-06-29
/*
* Given an array of 2n integers, your task is to group these integers into n pairs of integer, say (a1, b1), (a2, b2), ..., (an, bn) which makes sum of min(ai, bi) for all i from 1 to n as large as possible.
* Example 1:
* Input: [1,4,3,2]
* Output: 4
* Explanation: n is 2, and the maximum sum of pairs is 4 = min(1, 2) + min(3, 4).
* Note:
* 1. n is a positive integer, which is in the range of [1, 10000].
* 2. All the integers in the array will be in the range of [-10000, 10000].
*/

#include <stdio.h>
#include <stdlib.h>

int cmp(const void* a,const void* b)
{
    return (*(int*)a-*(int*)b);
}

int arrayPairSum(int* nums, int numsSize) {
    int sum=0;
    qsort(nums,numsSize,sizeof(int),cmp);
    for(int i=0;i<numsSize;i+=2){
        sum+=nums[i];
    }
    return sum;
}

int main(void)
{
    int nums[4]={1,4,3,2};
    printf("%d\n",arrayPairSum(nums,4));
    return 0;
}
