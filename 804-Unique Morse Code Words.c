// https://leetcode.com/problems/unique-morse-code-words/description/
// Date: 2018-06-26
/*
* International Morse Code defines a standard encoding where each letter is mapped to a series of dots and dashes, as follows: "a" maps to ".-", "b" maps to "-...", "c" maps to "-.-.", and so on.
* Now, given a list of words, each word can be written as a concatenation of the Morse code of each letter.
* For example, "cab" can be written as "-.-.-....-", (which is the concatenation "-.-." + "-..." + ".-"). We'll call such a concatenation, the transformation of a word.
* Return the number of different transformations among all words we have.
* Example:
* Input: words = ["gin", "zen", "gig", "msg"]
* Output: 2
* Explanation:
* The transformation of each word is:
* "gin" -> "--...-."
* "zen" -> "--...-."
* "gig" -> "--...--."
* "msg" -> "--...--."
* There are 2 different transformations, "--...-." and "--...--.".
* Note:
* The length of words will be at most 100.
* Each words[i] will have length in range [1, 12].
* words[i] will only consist of lowercase letters.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int uniqueMorseRepresentations(char** words, int wordsSize) {
    const char* morse[26]={
        ".-","-...","-.-.","-..",".","..-.","--.","....","..",".---","-.-",".-..","--",
        "-.","---",".--.","--.-",".-.","...","-","..-","...-",".--","-..-","-.--","--.."
    };
    char** trans=(char**)malloc(wordsSize*sizeof(char*));
    for(int i=0;i<wordsSize;++i){
        trans[i]=(char*)calloc(60,sizeof(char));
        for(int j=0;words[i][j]!='\0';++j){
            strcat(trans[i],morse[words[i][j]-'a']);
        }
    }
    int cnt=0;
    for(int i=0;i<wordsSize;++i){
        if(trans[i]!=NULL){
            ++cnt;
            for(int j=i+1;j<wordsSize;++j){
                if(trans[j]!=NULL&&strcmp(trans[i],trans[j])==0){
                    free(trans[j]);
                    trans[j]=NULL;
                }
            }
            free(trans[i]);
            trans[i]=NULL;
        }
    }
    free(trans);
    trans=NULL;
    return cnt;
}

int main(void)
{
    const char* words[4]={"gin", "zen", "gig", "msg"};
    printf("%d\n",uniqueMorseRepresentations((char**)words,4));
    return 0;
}
