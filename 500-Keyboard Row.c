// https://leetcode.com/problems/keyboard-row/description/
// Date: 2018-07-07
/*
* Given a List of words, return the words that can be typed using letters of alphabet on only one row's of American keyboard like the image below.
* Example 1:
* Input: ["Hello", "Alaska", "Dad", "Peace"]
* Output: ["Alaska", "Dad"]
* Note:
* 1. You may use one character in the keyboard more than once.
* 2. You may assume the input string will only contain letters of alphabet.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* kb[3]={"QWERTYUIOPqwertyuiop","ASDFGHJKLasdfghjkl","ZXCVBNMzxcvbnm"};
char hash_table[129];

void buildHashTable(void)
{
    static int call=0;
    if(call==0){
        ++call;
        memset(hash_table,0,129);
        for(int i=0;i<3;++i){
            for(int j=0;kb[i][j]!='\0';++j){
                hash_table[(int)kb[i][j]]=(char)(1<<i);
            }
        }
    }
    return;
}

char** findWords(char** words, int wordsSize, int* returnSize) {
    buildHashTable();
    char** ret=(char**)malloc(wordsSize*sizeof(char*));
    int ret_size=0;
    for(int i=0;i<wordsSize;++i){
        int kb_row=hash_table[(int)words[i][0]];
        int ok=1,len=1;
        for(int j=1;words[i][j]!='\0';++j){
            ++len;
            if(hash_table[(int)words[i][j]]!=kb_row){
                ok=0;
                break;
            }
        }
        if(ok){
            ret[ret_size]=(char*)malloc((len+10)*sizeof(char));
            memcpy(ret[ret_size],words[i],len+1);
            ++ret_size;
        }
    }
    *returnSize=ret_size;
    return ret;
}

int main(void)
{
    const char* words[4]={"Hello","Alaska","Dad","Peace"};
    int returnSize;
    char** ret=findWords((char**)words,4,&returnSize);
    for(int i=0;i<returnSize;++i){
        printf("%s\n",ret[i]);
        free(ret[i]);
        ret[i]=NULL;
    }
    free(ret);
    ret=NULL;
    return 0;
}
