// https://leetcode.com/problems/number-of-lines-to-write-string/description/
// Date: 2018-06-29
/*
* We are to write the letters of a given string S, from left to right into lines.
* Each line has maximum width 100 units, and if writing a letter would cause the width of the line to exceed 100 units, it is written on the next line.
* We are given an array widths, an array where widths[0] is the width of 'a', widths[1] is the width of 'b', ..., and widths[25] is the width of 'z'.
* Now answer two questions: how many lines have at least one character from S, and what is the width used by the last such line? Return your answer as an integer list of length 2.
* Example :
* Input:
* widths = [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
* S = "abcdefghijklmnopqrstuvwxyz"
* Output: [3, 60]
* Explanation:
* All letters have the same length of 10. To write all 26 letters, we need two full lines and one line with 60 units.
* Example :
* Input:
* widths = [4,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
* S = "bbbcccdddaaa"
* Output: [2, 4]
* Explanation:
* All letters except 'a' have the same length of 10, and "bbbcccdddaa" will cover 9 * 10 + 2 * 4 = 98 units.
* For the last 'a', it is written on the second line because there is only 2 units left in the first line.
* So the answer is 2 lines, plus 4 units in the second line.
*/

#include <stdio.h>
#include <stdlib.h>

int* numberOfLines(int* widths, int widthsSize, char* S, int* returnSize) {
    int* ret=(int*)calloc(2,sizeof(int));
    *returnSize=2;
    ret[0]=1;
    for(int i=0;S[i]!='\0';++i){
        int w=widths[S[i]-'a'];
        if(ret[1]+w<=100){
            ret[1]+=w;
        }else{
            ++ret[0];
            ret[1]=w;
        }
    }
    return ret;
}

int main(void)
{
    int returnSize;
    int widths[26]={10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10};
    const char* S="abcdefghijklmnopqrstuvwxyz";
    int* ret=numberOfLines(widths,26,(char*)S,&returnSize);
    printf("%d %d\n",ret[0],ret[1]);
    free(ret);
    ret=NULL;
    return 0;
}
