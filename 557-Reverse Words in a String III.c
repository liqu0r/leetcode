// https://leetcode.com/problems/reverse-words-in-a-string-iii/description/
// Date: 2018-07-07
/*
* Given a string, you need to reverse the order of characters in each word within a sentence while still preserving whitespace and initial word order.
* Example 1:
* Input: "Let's take LeetCode contest"
* Output: "s'teL ekat edoCteeL tsetnoc"
* Note: In the string, each word is separated by single space and there will not be any extra space in the string.
*/

#include <stdio.h>
#include <string.h>

void reverseString(char* begin,char* end)
{
    char tmp;
    while(begin<end){
        tmp=*begin;
        *begin=*end;
        *end=tmp;
        ++begin;
        --end;
    }
    return;
}

char* reverseWords(char* s) {
    if(strlen(s)==0){
        return s;
    }
    char *blank,*ret=s;
    while(1){
        blank=s+1;
        while(*blank!=' '&&*blank!='\0'){
            ++blank;
        }
        reverseString(s,blank-1);
        if(*blank=='\0'){
            break;
        }
        s=blank+1;
    }
    return ret;
}

int main(void)
{
    char s[]="Let's take LeetCode contest";
    printf("%s\n",reverseWords(s));
    return 0;
}
