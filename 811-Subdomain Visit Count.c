// https://leetcode.com/problems/subdomain-visit-count/description/
// Date: 2018-06-29
/*
* A website domain like "discuss.leetcode.com" consists of various subdomains.
* At the top level, we have "com", at the next level, we have "leetcode.com", and at the lowest level, "discuss.leetcode.com".
* When we visit a domain like "discuss.leetcode.com", we will also visit the parent domains "leetcode.com" and "com" implicitly.
* Now, call a "count-paired domain" to be a count (representing the number of visits this domain received), followed by a space, followed by the address.
* An example of a count-paired domain might be "9001 discuss.leetcode.com".
* We are given a list cpdomains of count-paired domains.
* We would like a list of count-paired domains, (in the same format as the input, and in any order), that explicitly counts the number of visits to each subdomain.
* Example 1:
* Input: ["9001 discuss.leetcode.com"]
* Output: ["9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com"]
* Explanation:
* We only have one website domain: "discuss.leetcode.com". As discussed above, the subdomain "leetcode.com" and "com" will also be visited. So they will all be visited 9001 times.
* Example 2:
* Input: ["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]
* Output: ["901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com"]
* Explanation:
* We will visit "google.mail.com" 900 times, "yahoo.com" 50 times, "intel.mail.com" once and "wiki.org" 5 times. For the subdomains, we will visit "mail.com" 900 + 1 = 901 times, "com" 900 + 50 + 1 = 951 times, and "org" 5 times.
* Notes:
* The length of cpdomains will not exceed 100.
* The length of each domain name will not exceed 100.
* Each address will have either 1 or 2 "." characters.
* The input count in any count-paired domain will not exceed 10000.
* The answer output can be returned in any order.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    int cnt;
    char* addr;
}Data;

typedef struct list{
    Data cpdomain;
    struct list* next;
}List;

void cpdomainToData(Data* dat,const char* cpdomain)
{
    int cnt;
    char addr[110];
    memset(addr,0,sizeof(addr));
    sscanf(cpdomain,"%d %s",&cnt,addr);
    dat->cnt=cnt;
    int addr_len=strlen(addr)+1;
    dat->addr=(char*)malloc(addr_len*sizeof(char));
    memcpy(dat->addr,addr,addr_len);
    return;
}

void ListPushback(List* ptr,const Data* dat)
{
    while(ptr->next!=NULL){
        ptr=ptr->next;
    }
    ptr->next=(List*)malloc(sizeof(List));
    memcpy(&(ptr->next->cpdomain),dat,sizeof(Data));
    ptr->next->next=NULL;
    return;
}

int ListLen(const List* ptr)
{
    int len=0;
    while(ptr!=NULL){
        ++len;
        ptr=ptr->next;
    }
    return len-1;
}

List* inList(List* ptr,const char* addr)
{
    ptr=ptr->next;
    while(ptr!=NULL){
        if(strcmp(addr,ptr->cpdomain.addr)==0){
            return ptr;
        }
        ptr=ptr->next;
    }
    return NULL;
}

List* getListNode(List* ptr,int n)
{
    ptr=ptr->next;
    int cnt=0;
    while(ptr!=NULL&&cnt<n){
        ptr=ptr->next;
        ++cnt;
    }
    return ptr;
}

void freeList(List* ptr)
{
    List* tmp;
    while(ptr!=NULL){
        tmp=ptr->next;
        free(ptr->cpdomain.addr);
        free(ptr);
        ptr=tmp;
    }
    return;
}

void strlsh(char* _Str)
{
    for(int i=0;_Str[i]!='\0';++i){
        _Str[i]=_Str[i+1];
    }
    return;
}

void strnlsh(char* _Str,int _Num)
{
    for(int i=0;i<_Num;++i){
        strlsh(_Str);
    }
    return;
}

void parentDomain(char* domain)
{
    Data tmp;
    cpdomainToData(&tmp,domain);
    for(int i=0;tmp.addr[i]!='\0';++i){
        if(tmp.addr[i]=='.'){
            strnlsh(tmp.addr,i+1);
            break;
        }
    }
    sprintf(domain,"%d %s",tmp.cnt,tmp.addr);
    free(tmp.addr);
    tmp.addr=NULL;
    return;
}

int isTopDomain(const char* domain)
{
    for(int i=0;domain[i]!='\0';++i){
        if(domain[i]=='.'){
            return 0;
        }
    }
    return 1;
}

int numLen(int num)
{
    int len=0;
    while(num){
        num/=10;
        ++len;
    }
    return len;
}

char** subdomainVisits(char** cpdomains, int cpdomainsSize, int* returnSize) {
    List* list_ptr=(List*)calloc(1,sizeof(List)),*location=NULL;
    Data tmp;
    for(int i=0;i<cpdomainsSize;++i){
        while(1){
            cpdomainToData(&tmp,cpdomains[i]);
            location=inList(list_ptr,tmp.addr);
            if(location==NULL){
                ListPushback(list_ptr,&tmp);
            }else{
                location->cpdomain.cnt+=tmp.cnt;
            }
            free(tmp.addr);
            tmp.addr=NULL;
            if(isTopDomain(cpdomains[i])){
                break;
            }else{
                parentDomain(cpdomains[i]);
            }
        }
    }
    *returnSize=ListLen(list_ptr);
    char** ret=(char**)malloc((*returnSize)*sizeof(char*));
    for(int i=0;i<*returnSize;++i){
        List* node=getListNode(list_ptr,i);
        int retLen=numLen(node->cpdomain.cnt)+strlen(node->cpdomain.addr)+2;
        ret[i]=(char*)malloc(retLen*sizeof(char));
        sprintf(ret[i],"%d %s",node->cpdomain.cnt,node->cpdomain.addr);
    }
    freeList(list_ptr);
    list_ptr=NULL;
    return ret;
}

int main(void)
{
    int returnSize;
    char* cpdomains[4];
    for(int i=0;i<4;++i){
        cpdomains[i]=(char*)malloc(20*sizeof(char));
    }
    strcpy(cpdomains[0],"900 google.mail.com");
    strcpy(cpdomains[1],"50 yahoo.com");
    strcpy(cpdomains[2],"1 intel.mail.com");
    strcpy(cpdomains[3],"5 wiki.org");
    char** ret=subdomainVisits(cpdomains,4,&returnSize);
    for(int i=0;i<returnSize;++i){
        printf("%s\n",ret[i]);
        free(ret[i]);
        ret[i]=NULL;
    }
    free(ret);
    ret=NULL;
    for(int i=0;i<4;++i){
        free(cpdomains[i]);
        cpdomains[i]=NULL;
    }
    return 0;
}
