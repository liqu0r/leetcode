// https://leetcode.com/problems/self-dividing-numbers/description/
// Date: 2018-06-29
/*
* A self-dividing number is a number that is divisible by every digit it contains.
* For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.
* Also, a self-dividing number is not allowed to contain the digit zero.
* Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.
* Example 1:
* Input: left = 1, right = 22
* Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]
* Note:
* The boundaries of each input argument are 1 <= left <= right <= 10000.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int* selfDividingNumbers(int left, int right, int* returnSize) {
    int* ret=(int*)malloc((right-left)*sizeof(int));
    int* ptr=ret;
    *returnSize=0;
    for(int i=left;i<=right;++i){
        int digit[10];
        memset(digit,0,sizeof(digit));
        int num=i,*d=digit;
        while(num){
            *d=num%10;
            if(*d==0){
                num=-1;
                break;
            }
            num/=10;
            ++d;
        }
        *d=-1;
        if(num!=-1){
            for(int j=0;digit[j]!=-1;++j){
                if(i%digit[j]!=0){
                    num=-1;
                    break;
                }
            }
        }
        if(num!=-1){
            *ptr++=i;
            ++(*returnSize);
        }
    }
    return ret;
}

int main(void)
{
    int returnSize;
    int* ret=selfDividingNumbers(1,22,&returnSize);
    for(int i=0;i<returnSize;++i){
        printf("%d ",ret[i]);
    }
    free(ret);
    ret=NULL;
    return 0;
}
