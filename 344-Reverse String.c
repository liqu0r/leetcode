// https://leetcode.com/problems/reverse-string/description/
// Date: 2018-07-07
/*
* Write a function that takes a string as input and returns the string reversed.
* Example:
* Given s = "hello", return "olleh".
*/

#include <stdio.h>
#include <string.h>

char* reverseString(char* s) {
    char tmp;
    for(int i=0,j=strlen(s)-1;i<j;++i,--j){
        tmp=s[i];
        s[i]=s[j];
        s[j]=tmp;
    }
    return s;
}

int main(void)
{
    char s[]="hello";
    printf("%s\n",reverseString(s));
    return 0;
}
