// https://leetcode.com/problems/flipping-an-image/description/
// Date: 2018-06-27
/*
* Given a binary matrix A, we want to flip the image horizontally, then invert it, and return the resulting image.
* To flip an image horizontally means that each row of the image is reversed.
* For example, flipping [1, 1, 0] horizontally results in [0, 1, 1].
* To invert an image means that each 0 is replaced by 1, and each 1 is replaced by 0.
* For example, inverting [0, 1, 1] results in [1, 0, 0].
* Example 1:
* Input: [[1,1,0],[1,0,1],[0,0,0]]
* Output: [[1,0,0],[0,1,0],[1,1,1]]
* Explanation: First reverse each row: [[0,1,1],[1,0,1],[0,0,0]].
* Then, invert the image: [[1,0,0],[0,1,0],[1,1,1]]
* Example 2:
* Input: [[1,1,0,0],[1,0,0,1],[0,1,1,1],[1,0,1,0]]
* Output: [[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]
* Explanation: First reverse each row: [[0,0,1,1],[1,0,0,1],[1,1,1,0],[0,1,0,1]].
* Then invert the image: [[1,1,0,0],[0,1,1,0],[0,0,0,1],[1,0,1,0]]
* Notes:
* 1 <= A.length = A[0].length <= 20
* 0 <= A[i][j] <= 1
*/

#include <stdio.h>
#include <stdlib.h>

int** flipAndInvertImage(int** A, int ARowSize, int *AColSizes, int** columnSizes, int* returnSize) {
    int** ret=(int**)malloc(ARowSize*sizeof(int*));
    *returnSize=ARowSize;
    *columnSizes=(int*)malloc(ARowSize*sizeof(int));
    for(int i=0;i<ARowSize;++i){
        (*columnSizes)[i]=AColSizes[i];
        ret[i]=(int*)malloc(AColSizes[i]*sizeof(int));
        for(int j=0;j<AColSizes[i];++j){
            ret[i][j]=A[i][AColSizes[i]-1-j]^1;
        }
    }
    return ret;
}

int main(void)
{
    int** A=(int**)malloc(3*sizeof(int));
    for(int i=0;i<3;++i){
        A[i]=(int*)calloc(3,sizeof(int));
    }
    A[0][0]=A[0][1]=A[1][0]=A[1][2]=1;
    int AColSizes[3]={3,3,3};
    int *columnSizes,returnSize;
    int** ret=flipAndInvertImage((int**)A,3,AColSizes,&columnSizes,&returnSize);
    for(int i=0;i<3;++i){
        for(int j=0;j<columnSizes[i];++j){
            printf("%d",ret[i][j]);
        }
        free(ret[i]);
        ret[i]=NULL;
        putchar('\n');
    }
    free(columnSizes);
    columnSizes=NULL;
    for(int i=0;i<3;++i){
        free(A[i]);
        A[i]=NULL;
    }
    free(A);
    A=NULL;
    return 0;
}
